{-# LANGUAGE LambdaCase #-}
module Chess.SAN
    ()
where

import           Chess.Pieces                   ( Figurine )
import qualified Chess.Pieces                  as Pieces

-- | Character representation of a piece in SAN.
--
-- The Pawn does not have a character representation.
figurineChar :: Figurine -> Maybe Char
figurineChar = \case
    Pieces.King   -> Just 'K'
    Pieces.Queen  -> Just 'Q'
    Pieces.Rook   -> Just 'R'
    Pieces.Bishop -> Just 'B'
    Pieces.Knight -> Just 'N'
    Pieces.Pawn   -> Nothing
