{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies      #-}
module Chess.AnsiBoard where

import           Data.Char                      ( chr )
import           Data.Text                      ( Text )
import qualified Data.Text                     as Text
import           Optics                         ( (^.) )
import           Optics.At                      ( at' )

import           Chess.Board                    ( ChessBoard
                                                , Rank
                                                , Square
                                                )
import qualified Chess.Board                   as Board
import           Chess.Pieces                   ( Piece )
import qualified Chess.Pieces                  as Pieces

test :: Text
test = boardToText Board.startingMapBoard

boardToText :: (ChessBoard board) => board -> Text
boardToText board =
    Text.intercalate "\n" $ rankToText board <$> reverse [minBound .. maxBound]

rankToText :: (ChessBoard board) => board -> Rank -> Text
rankToText board rank = mconcat $ renderSquare <$> squares
  where
    renderSquare :: Square -> Text
    renderSquare square = squareColor square $ case board ^. at' square of
        Nothing    -> "   "
        Just piece -> Text.pack [' ', pieceToChar piece, ' ']

    squares :: [Square]
    squares = flip Board.Square rank <$> [minBound .. maxBound]

    squareColor :: Square -> Text -> Text
    squareColor (Board.Square f r) = case (fromEnum f + fromEnum r) `mod` 2 of
        0 -> blackSquare
        1 -> whiteSquare
        _ -> error "should be 1 or 0"

pieceToChar :: Piece -> Char
pieceToChar = \case
    Pieces.Piece Pieces.White Pieces.King   -> uWhiteChessKing
    Pieces.Piece Pieces.White Pieces.Queen  -> uWhiteChessQueen
    Pieces.Piece Pieces.White Pieces.Rook   -> uWhiteChessRook
    Pieces.Piece Pieces.White Pieces.Bishop -> uWhiteChessBishop
    Pieces.Piece Pieces.White Pieces.Knight -> uWhiteChessKnight
    Pieces.Piece Pieces.White Pieces.Pawn   -> uWhiteChessPawn
    Pieces.Piece Pieces.Black Pieces.King   -> uBlackChessKing
    Pieces.Piece Pieces.Black Pieces.Queen  -> uBlackChessQueen
    Pieces.Piece Pieces.Black Pieces.Rook   -> uBlackChessRook
    Pieces.Piece Pieces.Black Pieces.Bishop -> uBlackChessBishop
    Pieces.Piece Pieces.Black Pieces.Knight -> uBlackChessKnight
    Pieces.Piece Pieces.Black Pieces.Pawn   -> uBlackChessPawn

whiteSquare :: Text -> Text
whiteSquare txt = mconcat
    [ -- foreground
      "\x001b[38;5;"
    , fgCode
    , "m"
      -- background
    , "\x001b[48;5;"
    , whiteSquareCode
    , "m"
    , txt
    , reset
    ]

blackSquare :: Text -> Text
blackSquare txt = mconcat
    [ -- foreground
      "\x001b[38;5;"
    , fgCode
    , "m"
      -- background
    , "\x001b[48;5;"
    , blackSquareCode
    , "m"
    , txt
    , reset
    ]

reset :: Text
reset = "\x001b[0m"

fgCode :: Text
fgCode = "232"

whiteSquareCode :: Text
whiteSquareCode = "255"

blackSquareCode :: Text
blackSquareCode = "247"

uWhiteChessKing :: Char
uWhiteChessKing = chr 0x2654

uWhiteChessQueen :: Char
uWhiteChessQueen = chr 0x2655

uWhiteChessRook :: Char
uWhiteChessRook = chr 0x2656

uWhiteChessBishop :: Char
uWhiteChessBishop = chr 0x2657

uWhiteChessKnight :: Char
uWhiteChessKnight = chr 0x2658

uWhiteChessPawn :: Char
uWhiteChessPawn = chr 0x2659

uBlackChessKing :: Char
uBlackChessKing = chr 0x265A

uBlackChessQueen :: Char
uBlackChessQueen = chr 0x265B

uBlackChessRook :: Char
uBlackChessRook = chr 0x265C

uBlackChessBishop :: Char
uBlackChessBishop = chr 0x265D

uBlackChessKnight :: Char
uBlackChessKnight = chr 0x265E

uBlackChessPawn :: Char
uBlackChessPawn = chr 0x265F
