{-# LANGUAGE ConstraintKinds     #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}
module Chess.Board where

import           Data.Foldable                  ( foldl' )
import           Data.Map.Strict                ( Map )
import qualified Data.Map.Strict               as Map
                                                ( empty )
import           Optics                         ( (.~)
                                                , (^.)
                                                )
import           Optics.At                      ( At(at)
                                                , Index
                                                , IxValue
                                                , Ixed
                                                , at'
                                                )
import           Optics.Lens                    ( Lens'
                                                , lens
                                                )
import           Optics.Setter                  ( set' )

import           Chess.Pieces                   ( Figurine
                                                , Piece
                                                , Player
                                                )
import qualified Chess.Pieces                  as Pieces

data Rank = Rank1
    | Rank2
    | Rank3
    | Rank4
    | Rank5
    | Rank6
    | Rank7
    | Rank8
    deriving (Eq, Ord, Enum, Bounded)

data File = FileA
    | FileB
    | FileC
    | FileD
    | FileE
    | FileF
    | FileG
    | FileH
    deriving (Eq, Ord, Enum, Bounded)

data Square = Square File Rank
    deriving (Eq, Ord)

type Board b a = (Index b ~ Square, IxValue b ~ a, At b)

type ChessBoard b = Board b Piece

data BoardInstance a = Board
    { boardInstanceGet :: Square -> Maybe a
    , boardInstanceSet :: Square -> Maybe a -> BoardInstance a
    }

type instance Index (BoardInstance a) = Square

type instance IxValue (BoardInstance a) = a

instance Ixed (BoardInstance a)

instance At (BoardInstance a) where
    at = boardL

boardL :: Square -> Lens' (BoardInstance a) (Maybe a)
boardL square = lens get set
  where
    get :: BoardInstance a -> Maybe a
    get board = boardInstanceGet board square

    set :: BoardInstance a -> Maybe a -> BoardInstance a
    set board = boardInstanceSet board square

mapBoard :: forall a . Map Square a -> BoardInstance a
mapBoard boardMap = Board { boardInstanceGet, boardInstanceSet }
  where
    boardInstanceGet :: Square -> Maybe a
    boardInstanceGet square = boardMap ^. at' square

    boardInstanceSet :: Square -> Maybe a -> BoardInstance a
    boardInstanceSet square value = mapBoard $ (at' square .~ value) boardMap

emptyMapBoard :: BoardInstance a
emptyMapBoard = mapBoard Map.empty

startingMapBoard :: BoardInstance Piece
startingMapBoard = setup emptyMapBoard
  where
    setup :: BoardInstance Piece -> BoardInstance Piece
    setup =
        place FileA Rank1 Pieces.White Pieces.Rook
            . place FileB Rank1 Pieces.White Pieces.Knight
            . place FileC Rank1 Pieces.White Pieces.Bishop
            . place FileD Rank1 Pieces.White Pieces.Queen
            . place FileE Rank1 Pieces.White Pieces.King
            . place FileF Rank1 Pieces.White Pieces.Bishop
            . place FileG Rank1 Pieces.White Pieces.Knight
            . place FileH Rank1 Pieces.White Pieces.Rook
            . place FileA Rank8 Pieces.Black Pieces.Rook
            . place FileB Rank8 Pieces.Black Pieces.Knight
            . place FileC Rank8 Pieces.Black Pieces.Bishop
            . place FileD Rank8 Pieces.Black Pieces.Queen
            . place FileE Rank8 Pieces.Black Pieces.King
            . place FileF Rank8 Pieces.Black Pieces.Bishop
            . place FileG Rank8 Pieces.Black Pieces.Knight
            . place FileH Rank8 Pieces.Black Pieces.Rook
            . placePawns Rank2 Pieces.White
            . placePawns Rank7 Pieces.Black

    placePawns :: Rank -> Player -> BoardInstance Piece -> BoardInstance Piece
    placePawns r p b = foldl'
        (\bi f -> f bi)
        b
        ((\f -> place f r p Pieces.Pawn) <$> [minBound .. maxBound])

    place
        :: File
        -> Rank
        -> Player
        -> Figurine
        -> BoardInstance Piece
        -> BoardInstance Piece
    place f r p fig = set' (at' (Square f r)) (Just (Pieces.Piece p fig))

