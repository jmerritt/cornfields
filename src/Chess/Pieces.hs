module Chess.Pieces
    ( Piece(..)
    , Figurine(..)
    , Player(..)
    )
where

data Piece = Piece Player Figurine

data Figurine = King
    | Queen
    | Rook
    | Bishop
    | Knight
    | Pawn
    deriving (Show)

data Player = White
    | Black
