{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}
module Chess.DrawBoard where

import           Codec.Picture                  ( PixelRGBA8(PixelRGBA8)
                                                , encodePng
                                                )
import qualified Data.ByteString.Lazy.Char8    as LBS8
import           Graphics.Rasterific            ( Cap(CapStraight)
                                                , Drawing
                                                , Geometry
                                                , Join(JoinMiter)
                                                , Path(Path)
                                                , PathCommand
                                                    ( PathCubicBezierCurveTo
                                                    , PathLineTo
                                                    )
                                                , PointSize(PointSize)
                                                , Primitive
                                                , V2(V2)
                                                , circle
                                                , fill
                                                , line
                                                , printTextAt
                                                , rectangle
                                                , renderDrawing
                                                , withTexture
                                                , withTransformation
                                                )
import           Graphics.Rasterific.Outline    ( strokize )
import           Graphics.Rasterific.Texture    ( uniformTexture )
import           Graphics.Rasterific.Transformations
                                                ( Transformation
                                                , scale
                                                , translate
                                                )
import           Graphics.Text.TrueType         ( BoundingBox
                                                    ( _xMax
                                                    , _xMin
                                                    , _yMax
                                                    , _yMin
                                                    )
                                                , Dpi
                                                , Font
                                                , loadFontFile
                                                , stringBoundingBox
                                                )
import           ITermShow                      ( displayImage )
import           Optics                         ( (^.) )
import           Optics.At                      ( at' )

import           Chess.Board                    ( ChessBoard
                                                , File
                                                , Rank
                                                , Square(Square)
                                                , startingMapBoard
                                                )
import           Chess.Pieces                   ( Figurine
                                                    ( Bishop
                                                    , King
                                                    , Knight
                                                    , Pawn
                                                    , Queen
                                                    , Rook
                                                    )
                                                , Piece(Piece)
                                                , Player(Black, White)
                                                )

testBoard :: IO ()
testBoard = do
    Right font <- loadFontFile "resources/fonts/opensans-regular.ttf"
    let textTheme = BoardTextTheme { boardTextThemeColor = PixelRGBA8 0 0 0 255
                                   , boardTextThemeFont      = font
                                   , boardTextThemePointSize = PointSize 18
                                   , boardTextThemeMargin    = 40
                                   }
        theme = defaultTheme
            { themeBoard = defaultBoardTheme
                               { boardThemeTextTheme = Just textTheme
                               }
            }
    let white                    = PixelRGBA8 255 255 255 255
        board                    = startingMapBoard
        (drawing, sz, transform) = gameDrawing theme board
        drawing'                 = withTransformation transform drawing
        image                    = renderDrawing sz sz white drawing'
    LBS8.putStrLn . displayImage . encodePng $ image

---- Drawing everything

data Theme px
  = Theme
    { themeBoard :: BoardTheme px
    , themePiece :: PieceTheme px
    }

defaultTheme :: Theme PixelRGBA8
defaultTheme =
    Theme { themeBoard = defaultBoardTheme, themePiece = defaultPieceTheme }

gameDrawing
    :: forall px board
     . (ChessBoard board)
    => Theme px
    -> board
    -> (Drawing px (), Int, Transformation)
gameDrawing theme board = (boardD <> piecesD, size, transformation)
  where
    boardD :: Drawing px ()
    size :: Int
    transformation :: Transformation
    (boardD, size, transformation) = boardDrawing (themeBoard theme)

    piecesD :: Drawing px ()
    piecesD = piecesDrawing (themeBoard theme) (themePiece theme) board

---- Drawing the Board

data BoardTextTheme px = BoardTextTheme
  { boardTextThemeColor     :: px
  , boardTextThemeFont      :: Font
  , boardTextThemePointSize :: PointSize
  , boardTextThemeMargin    :: Int
  }

data BoardTheme px = BoardTheme
  { boardThemeBlackColor      :: px
  , boardThemeWhiteColor      :: px
  , boardThemeLineColor       :: px
  , boardThemeSquareSize      :: Int
  , boardThemeLineThickness   :: Float
  , boardThemeBorderThickness :: Float
  , boardThemeTextTheme       :: Maybe (BoardTextTheme px)
  }

defaultBoardTheme :: BoardTheme PixelRGBA8
defaultBoardTheme = BoardTheme
    { boardThemeBlackColor      = PixelRGBA8 128 128 128 255
    , boardThemeWhiteColor      = PixelRGBA8 255 255 255 255
    , boardThemeLineColor       = PixelRGBA8 0 0 0 255
    , boardThemeSquareSize      = 50
    , boardThemeLineThickness   = 2
    , boardThemeBorderThickness = 4
    , boardThemeTextTheme       = Nothing
    }

piecesDrawing
    :: forall px board
     . (ChessBoard board)
    => BoardTheme px
    -> PieceTheme px
    -> board
    -> Drawing px ()
piecesDrawing theme pieceTheme board = mconcat pieces
  where
    pieces :: [Drawing px ()]
    pieces = pieceAtSquare <$> [ (i, j) | j <- [0 .. 7], i <- [0 .. 7] ]

    pieceAtSquare :: (Int, Int) -> Drawing px ()
    pieceAtSquare (i, j) = case board ^. at' (Square file rank) of
        Nothing    -> pure ()
        Just piece -> offset (i, j) (singlePiece piece)
      where
        file :: File
        file = toEnum i

        rank :: Rank
        rank = toEnum j

    offset :: (Int, Int) -> Drawing px () -> Drawing px ()
    offset (i, j) d = withTransformation (translate (V2 dx dy)) d
      where
        dx :: Float
        dx = squareSize * fromIntegral i

        dy :: Float
        dy = squareSize * fromIntegral (7 - j)

    singlePiece :: Piece -> Drawing px ()
    singlePiece piece =
        withTransformation (scale scaleFac scaleFac)
            $ pieceDrawing pieceTheme piece

    scaleFac :: Float
    scaleFac = squareSize / 100

    squareSize :: Float
    squareSize = fromIntegral . boardThemeSquareSize $ theme

pieceDrawing :: forall px . PieceTheme px -> Piece -> Drawing px ()
pieceDrawing theme piece = case piece of
    Piece player King   -> kingDrawing theme player
    Piece player Queen  -> queenDrawing theme player
    Piece player Bishop -> bishopDrawing theme player
    Piece player Knight -> knightDrawing theme player
    Piece player Rook   -> rookDrawing theme player
    Piece player Pawn   -> pawnDrawing theme player

boardDrawing
    :: forall px . BoardTheme px -> (Drawing px (), Int, Transformation)
boardDrawing theme = (drawing, sz, transformation)
  where
    drawing :: Drawing px ()
    drawing = squares <> labels

    transformation :: Transformation
    transformation = translate (V2 border border)

    squares :: Drawing px ()
    squares = boardSquaresDrawing theme

    labels :: Drawing px ()
    labels = boardLabelDrawing theme

    sz :: Int
    sz = round (boardSize + 2 * border)

    boardSize :: Float
    boardSize = fromIntegral (boardThemeSquareSize theme * 8)

    border :: Float
    border = case boardThemeTextTheme theme of
        Nothing -> boardThemeBorderThickness theme / 2
        Just tt -> fromIntegral (boardTextThemeMargin tt)

dpi :: Dpi
dpi = 96

boardLabelDrawing :: forall px . BoardTheme px -> Drawing px ()
boardLabelDrawing theme = case boardThemeTextTheme theme of
    Nothing -> pure ()
    Just tt -> filesTop <> filesBottom <> ranksLeft <> ranksRight
      where
        filesTop :: Drawing px ()
        filesTop = withTransformation (translate (V2 0 (-offset + h8))) files

        filesBottom :: Drawing px ()
        filesBottom = withTransformation
            (translate (V2 0 (boardSize + offset + h8)))
            files

        ranksLeft :: Drawing px ()
        ranksLeft = withTransformation (translate (V2 (-offset) h8)) ranks

        ranksRight :: Drawing px ()
        ranksRight =
            withTransformation (translate (V2 (boardSize + offset) h8)) ranks

        files :: Drawing px ()
        files = mconcat $ file <$> [0 .. 7]

        file :: Int -> Drawing px ()
        file i = drawTxt (V2 dx 0) (lbl i)
          where
            lbl :: Int -> String
            lbl 0 = "a"
            lbl 1 = "b"
            lbl 2 = "c"
            lbl 3 = "d"
            lbl 4 = "e"
            lbl 5 = "f"
            lbl 6 = "g"
            lbl 7 = "h"
            lbl _ = error "Invalid file index"

            dx :: Float
            dx = fromIntegral i * squareSize + squareSize / 2

        ranks :: Drawing px ()
        ranks = mconcat $ rank <$> [0 .. 7]

        rank :: Int -> Drawing px ()
        rank i = drawTxt (V2 0 dy) (show (8 - i))
          where
            dy :: Float
            dy = fromIntegral i * squareSize + squareSize / 2

        drawTxt :: V2 Float -> String -> Drawing px ()
        drawTxt (V2 x y) s =
            withTexture (uniformTexture (boardTextThemeColor tt)) $ printTextAt
                (boardTextThemeFont tt)
                (boardTextThemePointSize tt)
                (V2 (x - dx) y)
                s
          where
            dx :: Float
            dx = txtWidth s / 2

        h8 :: Float
        h8 = txtHeight "8" / 2

        txtHeight :: String -> Float
        txtHeight s = _yMax bb - _yMin bb
          where
            bb :: BoundingBox
            bb = bbox s

        txtWidth :: String -> Float
        txtWidth s = _xMax bb - _xMin bb
          where
            bb :: BoundingBox
            bb = bbox s

        bbox :: String -> BoundingBox
        bbox s = stringBoundingBox (boardTextThemeFont tt)
                                   dpi
                                   (boardTextThemePointSize tt)
                                   s

        squareSize :: Float
        squareSize = fromIntegral . boardThemeSquareSize $ theme

        boardSize :: Float
        boardSize = 8.0 * squareSize

        offset :: Float
        offset = fromIntegral (boardTextThemeMargin tt) / 2

boardSquaresDrawing :: forall px . BoardTheme px -> Drawing px ()
boardSquaresDrawing theme = allSquares <> vLines <> hLines <> border
  where
    border :: Drawing px ()
    border = withTexture
        (uniformTexture (boardThemeLineColor theme))
        (stroke' (boardThemeBorderThickness theme)
                 (JoinMiter 0.3)
                 (CapStraight 0, CapStraight 0)
                 (rectangle (V2 0 0) boardSize boardSize)
        )

    vLines :: Drawing px ()
    vLines = mconcat $ vLine <$> [1 .. 7]

    vLine :: Int -> Drawing px ()
    vLine i =
        withTexture (uniformTexture (boardThemeLineColor theme))
            $ withTransformation
                  (translate dv)
                  (stroke' (boardThemeLineThickness theme)
                           (JoinMiter 0.3)
                           (CapStraight 0, CapStraight 0)
                           (line (V2 0 0) (V2 0 boardSize))
                  )
      where
        dv :: V2 Float
        dv = V2 (squareSize * fromIntegral i) 0

    hLines :: Drawing px ()
    hLines = mconcat $ hLine <$> [1 .. 7]

    hLine :: Int -> Drawing px ()
    hLine i =
        withTexture (uniformTexture (boardThemeLineColor theme))
            $ withTransformation
                  (translate dv)
                  (stroke' (boardThemeLineThickness theme)
                           (JoinMiter 0.3)
                           (CapStraight 0, CapStraight 0)
                           (line (V2 0 0) (V2 boardSize 0))
                  )
      where
        dv :: V2 Float
        dv = V2 0 (squareSize * fromIntegral i)

    allSquares :: Drawing px ()
    allSquares =
        mconcat $ drSquare <$> [ (i, j) | j <- [0 .. 7], i <- [0 .. 7] ]

    drSquare :: (Int, Int) -> Drawing px ()
    drSquare (i, j) = xform $ case (i + j) `mod` 2 of
        0 -> whiteSquare
        1 -> blackSquare
        _ -> error "mod should be 0 or 1"
      where
        xform :: Drawing px () -> Drawing px ()
        xform = withTransformation (translate (V2 dx dy))

        dx :: Float
        dx = squareSize * fromIntegral i

        dy :: Float
        dy = squareSize * fromIntegral j

    whiteSquare :: Drawing px ()
    whiteSquare =
        withTexture (uniformTexture (boardThemeWhiteColor theme)) (fill square)

    blackSquare :: Drawing px ()
    blackSquare =
        withTexture (uniformTexture (boardThemeBlackColor theme)) (fill square)

    square :: [Primitive]
    square = rectangle (V2 0 0) squareSize squareSize

    boardSize :: Float
    boardSize = 8.0 * squareSize

    squareSize :: Float
    squareSize = fromIntegral . boardThemeSquareSize $ theme

---- Drawing pieces

data PieceTheme px = PieceTheme
    { pieceThemeOutlineColor      :: px
    , pieceThemeOutlineThickness  :: Float
    , pieceThemeBlackColor        :: px
    , pieceThemeWhiteColor        :: px
    , pieceThemeThinLineThickness :: Float
    }

defaultPieceTheme :: PieceTheme PixelRGBA8
defaultPieceTheme = PieceTheme
    { pieceThemeOutlineColor      = PixelRGBA8 0 0 0 255
    , pieceThemeOutlineThickness  = 3.0
    , pieceThemeBlackColor        = PixelRGBA8 0 0 0 255
    , pieceThemeWhiteColor        = PixelRGBA8 255 255 255 255
    , pieceThemeThinLineThickness = 2.0
    }

fillColor :: PieceTheme px -> Player -> px
fillColor theme player = case player of
    White -> pieceThemeWhiteColor theme
    Black -> pieceThemeBlackColor theme

fillAndStrokeFigurine :: PieceTheme px -> Player -> Path -> Drawing px ()
fillAndStrokeFigurine theme player path =
    fillFigurine theme player path <> strokeFigurine theme path

fillFigurine :: PieceTheme px -> Player -> Path -> Drawing px ()
fillFigurine theme player path = fillC (fillColor theme player) path

strokeFigurine :: PieceTheme px -> Path -> Drawing px ()
strokeFigurine theme path = strokeC (pieceThemeOutlineColor theme)
                                    (pieceThemeOutlineThickness theme)
                                    path

fillHighlights :: forall px . PieceTheme px -> Player -> [Path] -> Drawing px ()
fillHighlights theme player paths = mconcat $ fillSingleHighlight <$> paths
  where
    fillSingleHighlight :: Path -> Drawing px ()
    fillSingleHighlight = fillC hlFillColor

    hlFillColor :: px
    hlFillColor = case player of
        White -> pieceThemeBlackColor theme
        Black -> pieceThemeWhiteColor theme

strokeHighlightsThick
    :: forall px . PieceTheme px -> Player -> [Path] -> Drawing px ()
strokeHighlightsThick theme player paths = mconcat $ strokeSinglePath <$> paths
  where
    strokeSinglePath :: Path -> Drawing px ()
    strokeSinglePath = strokeC strokeColor (pieceThemeOutlineThickness theme)

    strokeColor :: px
    strokeColor = case player of
        White -> pieceThemeBlackColor theme
        Black -> pieceThemeWhiteColor theme

strokeHighlightsThin
    :: forall px . PieceTheme px -> Player -> [Path] -> Drawing px ()
strokeHighlightsThin theme player paths = mconcat $ strokeSinglePath <$> paths
  where
    strokeSinglePath :: Path -> Drawing px ()
    strokeSinglePath = strokeC strokeColor (pieceThemeThinLineThickness theme)

    strokeColor :: px
    strokeColor = case player of
        White -> pieceThemeBlackColor theme
        Black -> pieceThemeWhiteColor theme

---- King

kingDrawing :: forall px . PieceTheme px -> Player -> Drawing px ()
kingDrawing theme player =
    fillAndStrokeFigurine theme player kingOutline
        <> cross
        <> strokeHighlightsThick theme player kLineHighlights
  where
    cross :: Drawing px ()
    cross = strokeC (pieceThemeOutlineColor theme)
                    (pieceThemeOutlineThickness theme)
                    kingCross

    kLineHighlights :: [Path]
    kLineHighlights = case player of
        White -> kingWhiteLineHighlights
        Black -> kingBlackLineHighlights

kingOutline :: Path
kingOutline = Path
    (V2 50 25)
    True
    [ PathCubicBezierCurveTo (V2 56 25) (V2 60 34)  (V2 57 41)
    , PathCubicBezierCurveTo (V2 82 20) (V2 107 49) (V2 77 68)
    , PathLineTo (V2 76 86)
    , PathCubicBezierCurveTo (V2 69 89) (V2 59 90) (V2 50 90)
    , PathCubicBezierCurveTo (V2 41 90) (V2 31 89) (V2 24 86)
    , PathLineTo (V2 23 68)
    , PathCubicBezierCurveTo (V2 (-7) 49) (V2 18 20) (V2 43 41)
    , PathCubicBezierCurveTo (V2 40 34)   (V2 44 25) (V2 50 25)
    ]

kingCross :: [Path]
kingCross =
    [ Path (V2 44 14) False [PathLineTo (V2 56 14)]
    , Path (V2 50 25) False [PathLineTo (V2 50 8)]
    ]

kingWhiteLineHighlights :: [Path]
kingWhiteLineHighlights =
    [ Path (V2 57 41)
           False
           [PathCubicBezierCurveTo (V2 53 48) (V2 50 53) (V2 50 61)]
    , Path (V2 43 41)
           False
           [PathCubicBezierCurveTo (V2 47 48) (V2 50 53) (V2 50 61)]
    , Path
        (V2 77 68)
        False
        [ PathCubicBezierCurveTo (V2 72 64) (V2 63 61) (V2 50 61)
        , PathCubicBezierCurveTo (V2 37 61) (V2 28 64) (V2 23 68)
        ]
    , Path
        (V2 77 79)
        False
        [ PathCubicBezierCurveTo (V2 71 75) (V2 63 73) (V2 50 73)
        , PathCubicBezierCurveTo (V2 37 73) (V2 29 75) (V2 23 79)
        ]
    , Path
        (V2 76 86)
        False
        [ PathCubicBezierCurveTo (V2 71 82) (V2 63 79) (V2 50 79)
        , PathCubicBezierCurveTo (V2 37 79) (V2 29 82) (V2 24 86)
        ]
    ]

kingBlackLineHighlights :: [Path]
kingBlackLineHighlights =
    [ Path
        (V2 74 77)
        False
        [ PathCubicBezierCurveTo (V2 68 75) (V2 63 73) (V2 50 73)
        , PathCubicBezierCurveTo (V2 37 73) (V2 32 75) (V2 26 77)
        ]
    , Path
        (V2 73 84)
        False
        [ PathCubicBezierCurveTo (V2 68 81) (V2 63 79) (V2 50 79)
        , PathCubicBezierCurveTo (V2 37 79) (V2 32 81) (V2 27 84)
        ]
    , Path
        (V2 50 28)
        False
        [ PathCubicBezierCurveTo (V2 56 28) (V2 57 37) (V2 50 47)
        , PathCubicBezierCurveTo (V2 43 37) (V2 44 28) (V2 50 28)
        ]
    , Path
        (V2 53 63)
        False
        [ PathCubicBezierCurveTo (V2 53 58)   (V2 54 51)  (V2 61 42)
        , PathCubicBezierCurveTo (V2 76 27)   (V2 105 48) (V2 73 66)
        , PathCubicBezierCurveTo (V2 67 64)   (V2 59 63)  (V2 50 63)
        , PathCubicBezierCurveTo (V2 50 63)   (V2 33 63)  (V2 27 66)
        , PathCubicBezierCurveTo (V2 (-5) 48) (V2 24 27)  (V2 39 42)
        , PathCubicBezierCurveTo (V2 46 51)   (V2 47 58)  (V2 47 63)
        ]
    ]

---- Queen

queenDrawing :: forall px . PieceTheme px -> Player -> Drawing px ()
queenDrawing theme player =
    fillAndStrokeFigurine theme player queenOutline
        <> baubles
        <> strokeHighlightsThick theme player qLineHighlights
  where
    baubles :: Drawing px ()
    baubles =
        (mconcat $ fillC (fillColor theme player) <$> queenBaubles)
            <> (   mconcat
               $   strokeC (pieceThemeOutlineColor theme)
                           (pieceThemeOutlineThickness theme)
               <$> queenBaubles
               )

    qLineHighlights :: [Path]
    qLineHighlights = case player of
        White -> queenWhiteLineHighlights
        Black -> queenBlackLineHighlights

queenOutline :: Path
queenOutline = Path
    (V2 42 56)
    True
    [ PathLineTo (V2 50 17)
    , PathLineTo (V2 58 56)
    , PathLineTo (V2 71 20)
    , PathLineTo (V2 72 57)
    , PathLineTo (V2 89 28)
    , PathLineTo (V2 83 60)
    , PathCubicBezierCurveTo (V2 77 68) (V2 76 69) (V2 76 76)
    , PathCubicBezierCurveTo (V2 76 81) (V2 79 83) (V2 79 86)
    , PathCubicBezierCurveTo (V2 79 90) (V2 65 92) (V2 50 92)
    , PathCubicBezierCurveTo (V2 35 92) (V2 21 90) (V2 21 86)
    , PathCubicBezierCurveTo (V2 21 83) (V2 24 81) (V2 24 76)
    , PathCubicBezierCurveTo (V2 24 69) (V2 23 68) (V2 17 60)
    , PathLineTo (V2 11 28)
    , PathLineTo (V2 28 57)
    , PathLineTo (V2 29 20)
    , PathLineTo (V2 42 56)
    ]

queenBaubles :: [[Primitive]]
queenBaubles =
    [ bauble (V2 10 24)
    , bauble (V2 28 16)
    , bauble (V2 50 12)
    , bauble (V2 72 16)
    , bauble (V2 90 24)
    ]
  where
    bauble :: V2 Float -> [Primitive]
    bauble location = circle location 5.0

queenWhiteLineHighlights :: [Path]
queenWhiteLineHighlights =
    [ queenWhiteCorona
    , Path
        (V2 77 68)
        False
        [ PathCubicBezierCurveTo (V2 73 67) (V2 59 65) (V2 50 65)
        , PathCubicBezierCurveTo (V2 40 65) (V2 27 67) (V2 23 68)
        ]
    , Path
        (V2 76 77)
        False
        [ PathCubicBezierCurveTo (V2 71 76) (V2 59 74) (V2 50 74)
        , PathCubicBezierCurveTo (V2 41 74) (V2 29 76) (V2 24 77)
        ]
    , Path
        (V2 79 84)
        False
        [ PathCubicBezierCurveTo (V2 74 83) (V2 59 81) (V2 50 81)
        , PathCubicBezierCurveTo (V2 41 81) (V2 26 83) (V2 21 84)
        ]
    ]

queenBlackLineHighlights :: [Path]
queenBlackLineHighlights =
    [ Path
        (V2 73 67)
        False
        [ PathCubicBezierCurveTo (V2 69 66) (V2 59 65) (V2 50 65)
        , PathCubicBezierCurveTo (V2 41 65) (V2 31 66) (V2 27 67)
        ]
    , Path
        (V2 72 76)
        False
        [ PathCubicBezierCurveTo (V2 67 75) (V2 59 74) (V2 50 74)
        , PathCubicBezierCurveTo (V2 41 74) (V2 33 75) (V2 28 76)
        ]
    ]

queenWhiteCorona :: Path
queenWhiteCorona = Path
    (V2 17 60)
    False
    [ PathCubicBezierCurveTo (V2 19 59) (V2 21 58) (V2 22 57)
    , PathCubicBezierCurveTo (V2 26 59) (V2 32 58) (V2 35 55)
    , PathCubicBezierCurveTo (V2 38 57) (V2 45 58) (V2 50 55)
    , PathCubicBezierCurveTo (V2 55 58) (V2 62 57) (V2 65 55)
    , PathCubicBezierCurveTo (V2 68 58) (V2 74 59) (V2 78 57)
    , PathCubicBezierCurveTo (V2 79 58) (V2 81 59) (V2 83 60)
    ]

---- Bishop

bishopDrawing :: forall px . PieceTheme px -> Player -> Drawing px ()
bishopDrawing theme player =
    fillAndStrokeFigurine theme player bishopOutline
        <> strokeHighlightsThick theme player bLineHighlights
  where
    bLineHighlights :: [Path]
    bLineHighlights = case player of
        White -> bishopWhiteLineHighlights
        Black -> bishopBlackLineHighlights

bishopWhiteLineHighlights :: [Path]
bishopWhiteLineHighlights =
    bishopCross
        <> [ Path
               (V2 35 60)
               False
               [ PathCubicBezierCurveTo (V2 40 58) (V2 46 58) (V2 50 58)
               , PathCubicBezierCurveTo (V2 54 58) (V2 60 58) (V2 65 60)
               ]
           , Path
               (V2 33 68)
               False
               [ PathCubicBezierCurveTo (V2 37 67) (V2 45 66) (V2 50 66)
               , PathCubicBezierCurveTo (V2 55 66) (V2 63 67) (V2 67 68)
               ]
           ]

bishopBlackLineHighlights :: [Path]
bishopBlackLineHighlights =
    bishopCross
        <> [ Path
               (V2 38 59)
               False
               [ PathCubicBezierCurveTo (V2 41 58) (V2 46 58) (V2 50 58)
               , PathCubicBezierCurveTo (V2 54 58) (V2 59 58) (V2 62 59)
               ]
           , Path
               (V2 37 67)
               False
               [ PathCubicBezierCurveTo (V2 41 66) (V2 45 66) (V2 50 66)
               , PathCubicBezierCurveTo (V2 55 66) (V2 59 66) (V2 63 67)
               ]
           ]

bishopOutline :: Path
bishopOutline = Path
    (V2 50 85)
    True
    [ PathCubicBezierCurveTo (V2 53 88) (V2 61 90) (V2 65 90)
    , PathCubicBezierCurveTo (V2 69 90) (V2 73 89) (V2 76 89)
    , PathCubicBezierCurveTo (V2 81 89) (V2 83 89) (V2 86 92)
    , PathLineTo (V2 91 85)
    , PathCubicBezierCurveTo (V2 86 80) (V2 77 81) (V2 76 81)
    , PathCubicBezierCurveTo (V2 74 81) (V2 55 85) (V2 50 75)
    , PathCubicBezierCurveTo (V2 56 75) (V2 66 75) (V2 69 73)
    , PathCubicBezierCurveTo (V2 68 69) (V2 66 65) (V2 65 60)
    , PathCubicBezierCurveTo (V2 67 57) (V2 73 51) (V2 73 43)
    , PathCubicBezierCurveTo (V2 73 30) (V2 58 22) (V2 50 18)
    , PathCubicBezierCurveTo (V2 57 18) (V2 57 7)  (V2 50 7)
    , PathCubicBezierCurveTo (V2 43 7)  (V2 43 18) (V2 50 18)
    , PathCubicBezierCurveTo (V2 42 22) (V2 27 30) (V2 27 43)
    , PathCubicBezierCurveTo (V2 27 51) (V2 33 57) (V2 35 60)
    , PathCubicBezierCurveTo (V2 34 65) (V2 32 69) (V2 31 73)
    , PathCubicBezierCurveTo (V2 34 75) (V2 44 75) (V2 50 75)
    , PathCubicBezierCurveTo (V2 45 85) (V2 26 81) (V2 25 81)
    , PathCubicBezierCurveTo (V2 24 81) (V2 14 80) (V2 9 85)
    , PathLineTo (V2 14 92)
    , PathCubicBezierCurveTo (V2 17 89) (V2 19 89) (V2 24 89)
    , PathCubicBezierCurveTo (V2 27 89) (V2 31 90) (V2 35 90)
    , PathCubicBezierCurveTo (V2 39 90) (V2 47 88) (V2 50 85)
    ]

bishopCross :: [Path]
bishopCross =
    [ Path (V2 50 49) False [PathLineTo (V2 50 31)]
    , Path (V2 41 40) False [PathLineTo (V2 59 40)]
    ]

---- Knight

knightDrawing :: forall px . PieceTheme px -> Player -> Drawing px ()
knightDrawing theme player =
    fillAndStrokeFigurine theme player knightOutline
        <> fillHighlights theme player kFillHighlights
        <> strokeHighlightsThin theme player kLineHighlights
  where
    kFillHighlights, kLineHighlights :: [Path]
    (kFillHighlights, kLineHighlights) = case player of
        White -> (knightWhiteFillHighlights, knightWhiteLineHighlights)
        Black -> (knightBlackFillHighlights, knightBlackLineHighlights)

knightWhiteLineHighlights :: [Path]
knightWhiteLineHighlights =
    [ knightEyebrow
    , knightMouthWhite
    , knightForelockWhite
    , knightManeWhite
    , knightManeExtensionWhite
    ]

knightBlackLineHighlights :: [Path]
knightBlackLineHighlights =
    [knightEyebrow, knightMouthBlack, knightForelockBlack, knightCheekBlack]

knightWhiteFillHighlights :: [Path]
knightWhiteFillHighlights = [knightNostril, knightEye, knightManeWhite]

knightBlackFillHighlights :: [Path]
knightBlackFillHighlights = [knightNostril, knightEye, knightManeBlack]

knightOutline :: Path
knightOutline = Path
    (V2 50 20)
    True
    [ PathCubicBezierCurveTo (V2 58 16) (V2 89 24) (V2 89 90)
    , PathLineTo (V2 31 90)
    , PathCubicBezierCurveTo (V2 31 65) (V2 54 77) (V2 54 37)
    , PathCubicBezierCurveTo (V2 53 52) (V2 46 52) (V2 41 54)
    , PathCubicBezierCurveTo (V2 37 56) (V2 33 59) (V2 31 62)
    , PathCubicBezierCurveTo (V2 29 65) (V2 27 70) (V2 24 71)
    , PathCubicBezierCurveTo (V2 22 72) (V2 19 69) (V2 19 68)
    , PathCubicBezierCurveTo (V2 17 70) (V2 13 70) (V2 11 66)
    , PathCubicBezierCurveTo (V2 9 62)  (V2 9 57)  (V2 11 54)
    , PathCubicBezierCurveTo (V2 15 48) (V2 20 43) (V2 21 35)
    , PathCubicBezierCurveTo (V2 22 30) (V2 23 26) (V2 28 22)
    , PathCubicBezierCurveTo (V2 26 18) (V2 27 15) (V2 27 10)
    , PathCubicBezierCurveTo (V2 30 12) (V2 37 15) (V2 38 20)
    , PathCubicBezierCurveTo (V2 41 18) (V2 42 13) (V2 45 10)
    , PathCubicBezierCurveTo (V2 48 13) (V2 49 17) (V2 50 20)
    ]

knightNostril :: Path
knightNostril = Path
    (V2 17 55)
    True
    [ PathCubicBezierCurveTo (V2 15 55) (V2 13 58) (V2 13 59)
    , PathCubicBezierCurveTo (V2 13 60) (V2 15 62) (V2 16 62)
    , PathCubicBezierCurveTo (V2 17 62) (V2 20 59) (V2 20 58)
    , PathCubicBezierCurveTo (V2 20 57) (V2 18 55) (V2 17 55)
    ]

knightEye :: Path
knightEye = Path
    (V2 27 35)
    True
    [ PathCubicBezierCurveTo (V2 33 37) (V2 35 33) (V2 33 29)
    , PathCubicBezierCurveTo (V2 29 30) (V2 27 32) (V2 27 35)
    ]

knightEyebrow :: Path
knightEyebrow = Path
    (V2 26 37)
    False
    [PathCubicBezierCurveTo (V2 27 32) (V2 29 29) (V2 35 29)]

knightManeWhite :: Path
knightManeWhite = Path
    (V2 52 20)
    True
    [ PathCubicBezierCurveTo (V2 60 21) (V2 84 28) (V2 84 90)
    , PathLineTo (V2 89 90)
    , PathCubicBezierCurveTo (V2 89 30) (V2 64 19) (V2 54 19)
    ]

knightManeBlack :: Path
knightManeBlack = Path
    (V2 51 21)
    True
    [ PathCubicBezierCurveTo (V2 51 22) (V2 53 24) (V2 54 24)
    , PathCubicBezierCurveTo (V2 76 28) (V2 80 61) (V2 80 87)
    , PathLineTo (V2 87 87)
    , PathCubicBezierCurveTo (V2 87 40) (V2 66 16) (V2 51 21)
    ]

knightMouthWhite :: Path
knightMouthWhite = Path
    (V2 19 67)
    False
    [PathCubicBezierCurveTo (V2 20 67) (V2 23 63) (V2 24 62)]

knightMouthBlack :: Path
knightMouthBlack = Path
    (V2 20 67)
    False
    [PathCubicBezierCurveTo (V2 22 65) (V2 22 64) (V2 24 62)]

knightForelockWhite :: Path
knightForelockWhite = Path
    (V2 38 20)
    False
    [PathCubicBezierCurveTo (V2 36 22) (V2 35 23) (V2 33 24)]

knightForelockBlack :: Path
knightForelockBlack = Path
    (V2 38 20)
    False
    [PathCubicBezierCurveTo (V2 36 21) (V2 35 22) (V2 32 23)]

knightManeExtensionWhite :: Path
knightManeExtensionWhite = Path
    (V2 50 20)
    False
    [PathCubicBezierCurveTo (V2 49 21) (V2 49 21) (V2 48 22)]

knightCheekBlack :: Path
knightCheekBlack = Path
    (V2 46 49)
    False
    [PathCubicBezierCurveTo (V2 48 48) (V2 50 46) (V2 52 43)]

---- Rook

rookDrawing :: forall px . PieceTheme px -> Player -> Drawing px ()
rookDrawing theme player =
    fillAndStrokeFigurine theme player rookOutline
        <> strokeHighlightsThick theme player highlights
  where
    highlights :: [Path]
    highlights = case player of
        White -> rookWhiteHighlights
        Black -> rookBlackHighlights

rookOutline :: Path
rookOutline = Path
    (V2 85 90)
    True
    [ PathLineTo (V2 85 83)
    , PathLineTo (V2 85 83)
    , PathLineTo (V2 78 83)
    , PathLineTo (V2 78 76)
    , PathLineTo (V2 71 69)
    , PathLineTo (V2 71 34)
    , PathLineTo (V2 80 27)
    , PathLineTo (V2 80 15)
    , PathLineTo (V2 68 15)
    , PathLineTo (V2 68 21)
    , PathLineTo (V2 56 21)
    , PathLineTo (V2 56 15)
    , PathLineTo (V2 44 15)
    , PathLineTo (V2 44 21)
    , PathLineTo (V2 32 21)
    , PathLineTo (V2 32 15)
    , PathLineTo (V2 20 15)
    , PathLineTo (V2 20 27)
    , PathLineTo (V2 29 34)
    , PathLineTo (V2 29 69)
    , PathLineTo (V2 22 76)
    , PathLineTo (V2 22 83)
    , PathLineTo (V2 15 83)
    , PathLineTo (V2 15 90)
    ]

rookWhiteHighlights :: [Path]
rookWhiteHighlights =
    [ Path (V2 80 27) False [PathLineTo (V2 20 27)]
    , Path (V2 71 34) False [PathLineTo (V2 29 34)]
    , Path (V2 71 69) False [PathLineTo (V2 29 69)]
    , Path (V2 78 76) False [PathLineTo (V2 22 76)]
    , Path (V2 78 83) False [PathLineTo (V2 22 83)]
    ]

rookBlackHighlights :: [Path]
rookBlackHighlights =
    [ Path (V2 76 27) False [PathLineTo (V2 24 27)]
    , Path (V2 68 34) False [PathLineTo (V2 32 34)]
    , Path (V2 70 69) False [PathLineTo (V2 30 69)]
    , Path (V2 74 76) False [PathLineTo (V2 26 76)]
    , Path (V2 76 83) False [PathLineTo (V2 24 83)]
    ]

---- Pawn

pawnDrawing :: forall px . PieceTheme px -> Player -> Drawing px ()
pawnDrawing theme player = fillAndStrokeFigurine theme player pawnOutline

pawnOutline :: Path
pawnOutline = Path
    (V2 20 90)
    True
    [ PathLineTo (V2 80 90)
    , PathCubicBezierCurveTo (V2 79 72) (V2 72 62) (V2 60 57)
    , PathCubicBezierCurveTo (V2 69 51) (V2 70 35) (V2 56 31)
    , PathCubicBezierCurveTo (V2 63 26) (V2 60 15) (V2 50 15)
    , PathCubicBezierCurveTo (V2 40 15) (V2 37 26) (V2 44 31)
    , PathCubicBezierCurveTo (V2 30 35) (V2 31 51) (V2 40 57)
    , PathCubicBezierCurveTo (V2 28 62) (V2 21 72) (V2 20 90)
    ]

---- Utility functions

fillC :: Geometry geom => px -> geom -> Drawing px ()
fillC color geometry = withTexture (uniformTexture color) (fill geometry)

strokeC :: Geometry geom => px -> Float -> geom -> Drawing px ()
strokeC color width geometry = withTexture
    (uniformTexture color)
    (stroke' width (JoinMiter 0.3) (CapStraight 0, CapStraight 0) geometry)

-- Workaround for this bug:
--   https://github.com/Twinside/Rasterific/issues/56
stroke' :: Geometry geom => Float -> Join -> (Cap, Cap) -> geom -> Drawing px ()
stroke' width join caps geometry = fill $ strokize width join caps geometry
