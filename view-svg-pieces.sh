#!/usr/bin/env bash
#
# This script allows viewing the chess piece SVG files in a web browser. Run
# the script and then visit:
#   http://localhost:8080

declare -r port='8080'
declare -r docdir='./doc'
declare -r index='pieces.svg'

# Install wai-app-static if necessary
if ! stack exec which 'warp'
then
    stack build --copy-compiler-tool 'wai-app-static'
fi

# Run the server
stack exec warp --        \
      --port ${port}      \
      --docroot ${docdir} \
      --index ${index}
